#include <iostream>
#include <fstream>
#include <filesystem>

using namespace std;
namespace fs = filesystem;

void int_cin_fail_clear(int &choice);

int main() {
    string file_remove;
    fstream f;
    int choice, choice_2, file_amount;
    cout << "*******************************!!!UWAGA!!!******************************* \n"
         << "*  Pliki zostana utworzone lub usuniete dopiero po zamknieciu programu! * \n"
         << "************************************************************************* \n";
    do {
        cout << "1. Dodaj plik txt. \n"
             << "2. Wyswietl pliki z rozszerzeniem txt. \n"
             << "3. Usun pliki z rozszerzeniem txt. \n"
             << "4. Wyjdz z programu \n";
        cin >> choice;

        while (choice < 1 || choice > 4 || cin.fail()) {
            int_cin_fail_clear(choice);
        }

        if (choice == 1) {
            cout << "Podaj nazwe pliku. \n";
            string filename;
            cin >> filename;
            for (int i = 0; i < filename.length(); i++) {
                if (filename[i] == '/' || filename[i] == '\\' || filename[i] == ':' || filename[i] == '*' ||
                    filename[i] == '?' || filename[i] == '"' || filename[i] == '<' || filename[i] == '>' ||
                    filename[i] == '|') {
                    filename.erase(i, 1);
                    i--;
                }
            }
            if (ifstream(filename + ".txt")) {
                ofstream(filename + ".txt");
            } else {
                cout << "Plik o podanej nazwie juz istnieje. \n"
                     << "Czy chcesz go nadpisac? \n"
                     << "1. TAK \n"
                     << "2. NIE \n";
                cin >> choice_2;
                while (choice_2 != 1 && choice_2 != 2 || cin.fail()) {
                    int_cin_fail_clear(choice_2);
                }

                if (choice_2 == 1) {
                    ofstream(filename + ".txt");
                }
            }
        }

        if (choice == 2) {
            file_amount = 0;
            for (const auto &p : fs::directory_iterator(fs::current_path())) {
                if (p.path().extension() == ".txt" && p.path().filename() != "CMakeCache.txt") {
                    file_amount++;
                    cout << p.path().filename() << "\n";
                }
            }
            if (file_amount == 0) {
                cout << "W folderze nie ma plikow z rozszerzeniem txt. \n";
            }
        }

        if (choice == 3) {
            file_amount = 0;
            cout
                    << "Wpisz nazwe pliku, ktory chcesz usunac lub wpisz \"all\", aby usunac wszystkie pliki z rozszerzeniem txt. \n";
            cin >> file_remove;
            if (file_remove == "all") {
                for (const auto &p : fs::directory_iterator(fs::current_path())) {
                    if (p.path().extension() == ".txt" && p.path().filename() != "CMakeCache.txt") {
                        file_amount++;
                        fs::remove(p);
                    }
                }
                if (file_amount == 0) {
                    cout << "W folderze nie ma plikow z rozszerzeniem txt. \n";
                } else if (file_amount == 1) {
                    cout << "Usunieto " << file_amount << " plik. \n";
                } else if (file_amount < 5) {
                    cout << "Usunieto " << file_amount << " pliki. \n";
                } else {
                    cout << "Usunieto " << file_amount << " plikow. \n";
                }
            } else {
                file_remove.append(".txt");
                for (const auto &p : fs::directory_iterator(fs::current_path())) {
                    if (p.path().filename() == file_remove && p.path().filename() != "CMakeCache.txt") {
                        file_amount++;
                        fs::remove(p);
                    }
                }
                if (file_amount == 0) {
                    cout << "W folderze nie ma pliku o tej nazwie. \n";
                } else {
                    cout << "Usunieto plik \"" << file_remove << "\". \n";
                }
            }
        }
    } while (choice != 4);
    return 0;
}

void int_cin_fail_clear(int &choice) {
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    cout << "Wpisano niepoprawna wartosc \n";
    cin >> choice;
}
